
import os.path
import re
from pprint import pprint

def split_in_two(string, comma):
    ret = string.split(comma)
    if len(ret) >= 2:
        return (ret[0].strip(), ret[1].strip())
    return (ret[0].strip(),"")

class Function:
    def __init__(self, function, content):
        assert re.match(r"def .+\:.*", function), "function parameter doesn't match the function regex"
        self.__function = function
        self.__content = content

        p1 = function.find('(')
        p2 = function.find(')')
        rtype = function.find('->')

        self.__name = function[4:p1]
        self.__parameters = [x.strip() for x in function[p1 + 1: p2].split(',')]
        self.__return = function[rtype + 2:].strip() if rtype != -1 else ''

        dc1 = self.__content.find('"""')
        dc2 = self.__content[dc1 + 3:].find('"""')
        self.__docstring = self.__content[dc1 + 3: dc2 + 3].strip()
        self.__body = self.__content[dc2+6:]

    def get_content(self):
        return self.__content

    def get_name(self):
        return self.__name

    def get_parameters(self):
        return self.__parameters

    def get_rtype(self):
        return self.__return

    def __str__(self):
        return self.__function + '\n' + self.__content

    def get_docstring(self):
        return self.__docstring

    def __remake_docstring(self):
        remaked_docstring = '\t"""\nSome function definition \n'
        for param in self.__parameters:
            # Each param is of the form `myparam: "type;some definition" = default_value`

            p_name, p_data = split_in_two(param, ':')

            # In our example p_name = 'myparam' and p_data = '"some definition,type" = default_value'

            p_info, p_default = split_in_two(p_data, "=")

            # p_info = '"some definition,type"', p_default = 'default_value'

            p_info = p_info.replace('"', '')

            p_type, p_def = split_in_two(p_info, ";")

            remaked_docstring += ":parameter {}: {}".format(p_name, p_def)
            if p_default:
                remaked_docstring += "(default: {})".format(p_default)
            remaked_docstring += '\n'
            remaked_docstring += ":type {}: {}\n".format(p_name, p_type)

        f_return = self.__return.replace('"', '')
        f_rtype, f_rdef = split_in_two(f_return, ';')

        remaked_docstring += ":return: {}\n".format(f_rdef)
        remaked_docstring += ":rtype: {}\n".format(f_rtype)

        param_names = ', '.join([split_in_two(param, ":")[0] for param in self.__parameters])

        remaked_docstring += ":examples:\n\n"
        remaked_docstring += ">>> {}({})\n\n".format(self.__name, param_names)
        remaked_docstring += ">>> {}({})\n\n".format(self.__name, param_names)
        remaked_docstring += ">>> {}({})\n\n".format(self.__name, param_names)
        remaked_docstring += '"""\n'

        return remaked_docstring

    def get_function(self):
        new_function = ""

        param_names = ', '.join([split_in_two(param, ":")[0] for param in self.__parameters])

        new_function += "def {}({}):\n".format(self.__name, param_names)
        new_function += "\n\t".join(self.__remake_docstring().split('\n'))
        new_function += "\n\t".join(self.__body.split('\n'))

        return new_function


def from_python_file(path):
    assert os.path.exists(path), "File doesn't exist or you don't have access to it"

    file = open(path, 'r+')
    content = file.read()
    file.close()

    file_content = ""

    functions = re.findall(r"def .+\:.*", content)
    separated = []
    cursor = [0] # Equivalent of a stack
    # Non - intrusive : not modifying content
    for function in functions:
        f_content = ""
        i = content.find(function) + len(function)

        while True:
            if content[i+1:i+5] == '    ':
                next_line = i + 1 + content[i+1:].find('\n')
                f_content += content[i+5: next_line+1]
                i = next_line
            elif content[i+1] == '\n':
                i += 1
            else:
                file_content += content[cursor.pop():content.find(function)] + Function(function, f_content).get_function()
                cursor.append(i)
                break

         # separated.append(Function(function, f_content))

    last = cursor.pop()
    return file_content + content[last:]

if __name__ == "__main__":
    import sys
    assert len(sys.argv) == 2, "Only one argument accepted"
    path = sys.argv[1]

    from shutil import copyfile
    from os import makedirs
    from os.path import exists, isfile, join
    from ntpath import basename

    assert exists(path), "The file is not reachable (maybe mistyped ?)"
    assert isfile(path), "The given path is actually not a file !"

    # # During test, always restoring from backup
    #
    # copyfile(join("auto_backup", basename(path)), basename(path))

    # Creating backup
    if not exists("auto_backup"):
        makedirs('auto_backup')

    copyfile(basename(path), join("auto_backup", basename(path)))

    content = from_python_file(path)

    with open(path, 'w') as file:
        file.write(content)
