# Contributing


```
$ git clone https://Etilawin@bitbucket.org/Etilawin/autocomplete.git
$ git checkout -b develop origin/develop
```

Now two cases:

1. You create a new feature
```
$ git checkout -b your-new-feature-name develop
$ # Do your job :
$ git status
$ git add <some-file>
$ git commit
```

2. You end a feature
```
git pull origin develop
git checkout develop
git merge some-feature
git push
git branch -d some-feature
```

If ever you did not understand you can read more here:
https://stackoverflow.com/questions/38675829/how-to-create-releases-for-public-or-private-repository-in-github
https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow

If you are not used to Git you can use:
* On phone:
* https://play.google.com/store/apps/details?id=com.enki.insights
* Anywhere:
* https://fr.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud
