# Autocompleter

This project has for goal to auto document a python file. This, in the end,
should be able to make the documentation of any function and any class from
a "normal pattern" to the specified syntax.

## Getting Started

Just clone the project with the following command :
```
$ git clone https://Etilawin@bitbucket.org/Etilawin/autocomplete.git
```
This will be asking you the password, which is the one of your bitbucket account.

### Prerequisites

For the autocomplete.py to run you need python3 or higher, shutil, os and ntpath
which are normally auto installed with python.

Under Linux:
```
$ sudo apt-get install python3
```
Should do the job.

### Installing

If you want to install but NOT modify the project

```
$ git clone https://Etilawin@bitbucket.org/Etilawin/autocomplete.git
$ git checkout master
```

## Running the tests

Tests are not made yet

### Break down into end to end tests

Tests are not made yet

### And coding style tests

Tests are not made yet

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Python](https://docs.python.org/3/) - The python framework

## Contributing

Please read CONTRIBUTING.md for details.

## Versioning

The versionning follows this rule:

Given a version number MAJOR.MINOR.PATCH, increment the:

    MAJOR version when you make incompatible API changes,
    MINOR version when you add functionality in a backwards-compatible manner, and
    PATCH version when you make backwards-compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.

## Authors

* **Kim Vallée** - *Initial work* - [Etilawin](https://bitbucket.org/Etilawin/)

Other contributors will be added by the amount of work they provided to this project

## License

This project is licensed under the agpl-3.0 Licence - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

Nothing yet
